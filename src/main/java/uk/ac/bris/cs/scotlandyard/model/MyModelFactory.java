package uk.ac.bris.cs.scotlandyard.model;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYard.Factory;

import java.util.HashSet;
import java.util.*;
/**
 * cw-model
 * Stage 2: Complete this class
 */
public final class MyModelFactory implements Factory<Model> {


	@Nonnull @Override public Model build(GameSetup setup,
	                                      Player mrX,
	                                      ImmutableList<Player> detectives) {


		Set<Model.Observer> observers = new HashSet<>();

		MyGameStateFactory game = new MyGameStateFactory(); //create new game
		Board.GameState state = game.build(setup, mrX, detectives);


		Model model = new Model() {

			Board.GameState modelState = state;

			@Nonnull
			@Override
			public Board getCurrentBoard() {
				return modelState;
			}

			@Override
			public void registerObserver(@Nonnull Observer observer) {
					if (observer == null) throw new NullPointerException("Observer can't be null");

					for (Observer s : observers)
						if (s.equals(observer)) throw new IllegalArgumentException("Duplicate observer");

					observers.add(observer);
			}

			@Override
			public void unregisterObserver(@Nonnull Observer observer) {
			     	if (observer == null) throw new NullPointerException("Observer can't be null");

			     	if (observers.contains(observer)) observers.remove(observer);
						else throw new IllegalArgumentException("Can't unregister an inexistent observer");
			}

			@Nonnull
			@Override
			public ImmutableSet<Observer> getObservers() {
				return ImmutableSet.copyOf(observers);
			}

			@Override
			public void chooseMove(@Nonnull Move move) {
				modelState = modelState.advance(move);
				var event = modelState.getWinner().isEmpty() ? Observer.Event.MOVE_MADE : Observer.Event.GAME_OVER;
				for (Observer o : observers) o.onModelChanged(modelState, event);
			}
		};

		return model;
	}


}
