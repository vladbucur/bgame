package uk.ac.bris.cs.scotlandyard.model;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;
import uk.ac.bris.cs.scotlandyard.model.Board.GameState;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYard.Factory;


import java.util.Optional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * cw-model
 * Stage 1: Complete this class
 */


public final class MyGameStateFactory implements Factory<GameState> {


	private final class MyGameState implements GameState {
		private GameSetup setup;
		private ImmutableSet<Piece> remaining;
		private ImmutableList<LogEntry> log;
		private Player mrX;
		private List<Player> detectives;
		private ImmutableList<Player> everyone;
		private ImmutableSet<Move> moves;
		private ImmutableSet<Piece> winner;

		private MyGameState(final GameSetup setup,
							final ImmutableSet<Piece> remaining,
							final ImmutableList<LogEntry> log,
							final Player mrX,
							final List<Player> detectives){

			this.setup = setup;
			this.remaining = remaining;
			this.log = log;
			this.mrX = mrX;
			this.detectives = detectives;
			this.winner= ImmutableSet.of();
			this.moves = getAvailableMoves();
			this.winner = getWinner();
			this.everyone = ImmutableList.<Player>builder()
									.addAll(detectives)
									.add(mrX)
									.build();

		}

		@Nonnull
		@Override
		public GameState advance(Move move) {

			Piece who = move.commencedBy(); //which player is making the next move
			int s = move.source(); // player's initial position

			Set<Piece> newRemaining = new HashSet<>(); //the remaining set for the next state
			List<LogEntry> l = new ArrayList<>(); // the new LogEntry list
			List<Player> aux = new ArrayList<>(); // the

			l.addAll(getMrXTravelLog()); //adding the current log to the new one

			if (!moves.contains(move)) throw new IllegalArgumentException("Illegal move: " + move);

			// This list gets the destination(s) after the move is made.
			List<Integer> Destination = move.visit(new Move.Visitor<List<Integer>>() {
				@Override public List<Integer> visit(Move.SingleMove singleMove) {
					List<Integer> Destination = new ArrayList<>();
					Destination.add(singleMove.destination);
					return Destination;
				}
				@Override public List<Integer> visit(Move.DoubleMove doubleMove) {
					List <Integer> Destination = new ArrayList<>();
					Destination.add(doubleMove.destination1);
					Destination.add(doubleMove.destination2);
					return Destination;
				}
			} );

			int destination = Destination.iterator().next(); //extract the first element from the list
			int destination2 = -1; // we provide an invalid location in case we have only 1 location in the list

			if (Destination.size() > 1 && who.isMrX()) { //DoubleMove is valid only for mrX
				//extract the last/second element in the list
				int lastElement = -1; //an initial invalid location

				for (int last : Destination)
					lastElement = last;

				destination2 = lastElement;
			}

			Player mrX2 = mrX; //new mrX for the next state

			if(who.isMrX()) { //if is mrX's turn

				if (destination2 == -1) mrX2 = mrX.at(destination); //if it is a SingleMove
					else mrX2 = mrX.at(destination2); //else it is a DoubleMove

				mrX2 = mrX2.use(move.tickets());

				//if it is a doubleMove move.tickets() I need to extract the first 2 tickets
				ScotlandYard.Ticket lastTicket = null; //the second ticket

				if (destination2 != -1) {
					int counter = 0;
					//extracting the second ticket
					for (ScotlandYard.Ticket ticket : move.tickets()) {
						counter++;
						lastTicket = ticket;
						if (counter == 2) break;
					}
				}


				LogEntry x = null, y = null; // the 2 possible log entries


				if (setup.rounds.get(log.size())) x = LogEntry.reveal(move.tickets().iterator().next(), destination);
					else x = LogEntry.hidden(move.tickets().iterator().next());

				l.add(x); // add the first LogEntry

				if (lastTicket != null) {
					//use log.size() + 1 cause if it is a DoubleMove we have 2 entries
					if (setup.rounds.get(log.size() + 1)) y = LogEntry.reveal(lastTicket, destination2);
						else y = LogEntry.hidden(lastTicket);

					l.add(y); //add the second LogEntry
				}

				//preparing the remaining for the next state
				//mrX turn => I will add all the detectives which have at least 1 ticket
				for (var detective : detectives) {
					boolean ok = false;

					for (Integer value : detective.tickets().values())
						if (value > 0) ok = true;

					//the boolean checks if the detective has at least 1 ticket
					if (ok) newRemaining.add(detective.piece());

					aux.add(detective);
				}

			}

			else { //if it is a detectives' turn

				for (Player d : detectives)
					if (d.piece() == who) {
						 d = d.at(destination);
						 d = d.use(move.tickets());

						 aux.add(d); // the modified detective (less tickets, new location)

						 mrX2 = mrX.give(move.tickets());

						 //preparing the remaining for the next state
						 for (var i : remaining) {
							if (i == who) continue;
							newRemaining.add(i);
						}
					}
					else aux.add(d); // unmodified detective (same as in the last state)

				if (newRemaining.isEmpty()) newRemaining.add(mrX.piece()); //all the detective moved, it is time for mrX again
			}

			ImmutableSet <Piece> finalRemaining = ImmutableSet.copyOf(newRemaining);
			ImmutableList <LogEntry> finalLog = ImmutableList.copyOf(l);

			return new MyGameState(getSetup(), finalRemaining, finalLog, mrX2, aux);
		}

		@Nonnull
		@Override
		public GameSetup getSetup() {
			return setup;

		}

		@Nonnull
		@Override
		public ImmutableSet<Piece> getPlayers() {

			return ImmutableSet.<Piece>copyOf(
					everyone.stream().map(player -> player.piece()).collect(Collectors.toSet()) );

		}

		@Nonnull
		@Override
		public Optional<Integer> getDetectiveLocation(Piece.Detective detective) {

			for (final var player : detectives)
				if (player.piece() == detective) return Optional.of(player.location());

			return Optional.empty();
		}

		@Nonnull
		@Override
		public Optional<TicketBoard> getPlayerTickets(Piece piece) {

			for (Player player : everyone)
				if (player.piece() == piece) {
					TicketBoard x = new TicketBoard() { //anonymous inner class for implementing TicketBoard
						@Override
						public int getCount(@Nonnull ScotlandYard.Ticket ticket) {
							return player.tickets().get(ticket);
						}
					};

					return Optional.of(x);
				}

			return Optional.empty();
		}

		@Nonnull
		@Override
		public ImmutableList<LogEntry> getMrXTravelLog() {
			return log;
		}

		@Nonnull
		@Override
		public ImmutableSet<Piece> getWinner() {

			Set<Piece> a = new HashSet<>();
			boolean mrxCaught = false;

			//if is mrX turn and number of rounds == log.size() => mrX wins
			if (remaining.contains(mrX.piece()) && setup.rounds.size() <= log.size()) return ImmutableSet.of(mrX.piece());

			//if mrX is caught by a detective
			for (final var detective : detectives)
				if (mrX.location() == detective.location()) mrxCaught = true;

			//detectives win if mrX is caught or is mrX's turn and he doesn't have any available moves
			if ( mrxCaught || (moves.isEmpty() && remaining.contains(mrX.piece())) ) {

				for (final var d : detectives)
					a.add(d.piece());

				return ImmutableSet.copyOf(a);
			}

			boolean hasTickets = false;

			for (final var detective : detectives)
				for (final var countTickets : detective.tickets().values())
					if (countTickets > 0) hasTickets = true;

			//mrX wins if the detectives don't have anymore tickets or the detectives don't have any available moves
			if (!hasTickets || (moves.isEmpty() && !remaining.contains(mrX.piece())) ) return ImmutableSet.of(mrX.piece());

			return ImmutableSet.of(); //no winner

		}

		@Nonnull
		@Override
		public ImmutableSet<Move> getAvailableMoves() {

			if (!winner.isEmpty()) return ImmutableSet.of(); //if we have a winner => we don't have any available moves

			Set<Move> list = new HashSet<>(); //a list of all of the available moves

			// MISTER X TURN
			if (remaining.contains(mrX.piece())) {
				//DoubleMove for mrX
				if (mrX.has(ScotlandYard.Ticket.DOUBLE) && log.size() + 2 <= setup.rounds.size()) list.addAll(makeDoubleMove(setup, detectives, mrX, mrX.location()));

				//SingleMove for mrX
				list.addAll(makeSingleMoves(setup, detectives, mrX, mrX.location()));
			}

			// DETECTIVE TURN
			else {

				//just SingleMove
				for (final var detective : detectives)
					if (remaining.contains(detective.piece()))
						list.addAll(makeSingleMoves(setup, detectives, detective, detective.location()));

			}

			return ImmutableSet.copyOf(list);

		}

	}

	private static ImmutableSet<Move.SingleMove> makeSingleMoves(GameSetup setup, List<Player> detectives, Player player, int source) {

		Set<Move.SingleMove> singleMoves = new HashSet<>(); //a set with all possible single moves

		for (int destination : setup.graph.adjacentNodes(source)) {
			var occupied = false;

			//find out if destination is occupied by a detective
			for (final var detective : detectives) {
				if (detective.location() == destination) occupied = true;
			}

			if (occupied) continue; //if occupied skip

			//add moves if the player has the required ticket
			for (ScotlandYard.Transport t : setup.graph.edgeValueOrDefault(source, destination, ImmutableSet.of())) {
				if (player.has(t.requiredTicket()))
					singleMoves.add(new Move.SingleMove(player.piece(), source, t.requiredTicket(), destination));
			}

			//add moves to the destination via a Secret ticket if there are any left with the player
			if (player.isMrX() && player.has(ScotlandYard.Ticket.SECRET))
				for (ScotlandYard.Transport t : setup.graph.edgeValueOrDefault(source, destination, ImmutableSet.of()))
					singleMoves.add(new Move.SingleMove(player.piece(), source, ScotlandYard.Ticket.SECRET, destination));

		}

		return ImmutableSet.copyOf(singleMoves);
	}

	private static ImmutableSet<Move.DoubleMove> makeDoubleMove (GameSetup setup, List<Player> detectives, Player player, int source)  {

		ImmutableSet<Move.SingleMove> a = makeSingleMoves(setup, detectives, player, source); //a set with the possible first moves
		Set<Move.DoubleMove> x = new HashSet<>(); //a set with all the possible double moves

		for (Move.SingleMove mov : a) {
			//all the possible second moves using the initial first moves
			ImmutableSet<Move.SingleMove> b = makeSingleMoves(setup, detectives, player, mov.destination);

			ScotlandYard.Ticket whatIsTheFirstTicket = mov.ticket;

			for (Move.SingleMove mov2 : b) {
				if (mov2.ticket == whatIsTheFirstTicket && !player.hasAtLeast(whatIsTheFirstTicket, 2)) continue;
				Move.DoubleMove ticket = new Move.DoubleMove(player.piece(), source, mov.ticket, mov.destination, mov2.ticket, mov2.destination);
				x.add(ticket);
			}

		}

		return ImmutableSet.copyOf(x);
	}


	@Nonnull
	@Override
	public GameState build(
			GameSetup setup,
			Player mrX,
			ImmutableList<Player> detectives) {

		if (mrX == null) throw new NullPointerException("Mr X is missing");
		if (!mrX.isMrX()) throw new IllegalArgumentException("Mr X doesn't have a Mr X ticket");

		for (final var p : detectives) {
			if (p == null) throw new NullPointerException("Detective is missing");

			if (p.isMrX()) throw new IllegalArgumentException("The detective can't be also Mr. X");

			for (final var p2 : detectives)
				if (p != p2) {
					if (p.location() == p2.location())
						throw new IllegalArgumentException("2 detectives can't be on the same position");
					if (p.piece() == p2.piece())
						throw new IllegalArgumentException("2 detectives can't have the same colour");
				}

			if (p.has(ScotlandYard.Ticket.SECRET))
				throw new IllegalArgumentException("Detective can't have a secret ticket");

			if (p.has(ScotlandYard.Ticket.DOUBLE))
				throw new IllegalArgumentException("Detective can't have a double ticket");
		}

		if (setup.rounds.size() == 0) throw new IllegalArgumentException("Can't have 0 rounds");
		if (setup.graph.nodes().size() == 0) throw new IllegalArgumentException("Can't have an empty graph");

		return new MyGameState(setup, ImmutableSet.of(mrX.piece()),	ImmutableList.of(), mrX, detectives);

	}
	

}